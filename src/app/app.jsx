import React from 'react';
import ReactDOM from 'react-dom';

// const MainDom = require('./MainDom/MainDom.jsx');
const MovieSearchPage = require('./MovieSearchPage/MovieSearchPage.jsx');

class App extends React.Component {
    constructor (props) {
        super(props);
    }
    render () {
        return <div className="app container">
                <MovieSearchPage />
            </div>;
    }
}

ReactDOM.render(
    <App param="12"/>,
    document.getElementById('main-app')
);
