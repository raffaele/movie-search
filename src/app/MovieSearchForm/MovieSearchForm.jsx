import React from 'react';
import ReactDOM from 'react-dom';
import {FormGroup, ControlLabel, FormControl, FieldGroup, Button} from 'react-bootstrap';

class MovieSearchForm extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            query: ''
        };
    }

    render () {
        var context = this;
        function getValidationState () {}
        function handleChange (event) {
            context.setState({
                query: event.target.value
            });
        }
        function submitForm (event) {
            var onSubmitProps = context.props.onSubmit,
                query = context.state.query;
            event.preventDefault();
            if (onSubmitProps && query) {
                onSubmitProps(query);
            }
        }

        return <form className="movie-search-form" onSubmit={submitForm}>
                <FormGroup controlId="formBasicText" validationState={getValidationState()}>
                    <ControlLabel>Seach your movie</ControlLabel>
                    <FormControl type="text" value={this.state.query} placeholder="Enter text" onChange={handleChange} />
                </FormGroup>
                
                <Button type="submit">Go</Button>
            </form>;
    }
}

MovieSearchForm.propTypes = {
    onSubmit: React.PropTypes.func
};

module.exports = MovieSearchForm;
