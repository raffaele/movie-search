const { expect } = require('chai');
const { shallow } = require('enzyme');
const MovieSearchForm = require('./MovieSearchForm.jsx');
const React = require('react');
const sinon = require('sinon');

describe('<MovieSearchForm />', () => {
    var submitCallback,
        component;

    beforeEach(()=>{
        submitCallback = sinon.spy();
        component = shallow(<MovieSearchForm onSubmit={submitCallback} />);
    });
    it('should contain the form', () => {
        expect(component.find('form').length).to.be.equal(1);
    });

    describe('On form submit', () => {
        it('should not call the submit callback if the query is empty', () => {
            component.find('form').simulate('submit', {preventDefault: sinon.spy()});
            expect(submitCallback.called).to.be.equal(false);
        });

        it('should call the submit callback if the query is not empty', () => {
            var textInput = {
                target: {
                    value: 'ring'
                }
            };

            component.find('FormControl').simulate('change', textInput);
            component.find('form').simulate('submit', {preventDefault: sinon.spy()});
            expect(submitCallback.calledWith(textInput.target.value)).to.be.equal(true);
        });
    });

    
});
