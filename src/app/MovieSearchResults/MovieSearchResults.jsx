import React from 'react';
import ReactDOM from 'react-dom';
import {Modal, Panel, Button} from 'react-bootstrap';

const MovieListItem = require('../MovieListItem/MovieListItem.jsx'),
    theMovieDb = require('../../theMovieDb.js');

class MovieSearchForm extends React.Component {
    constructor (props) {
        super(props);
        this.state = {};
    }

    showMovieDetails (movieDetails) {
        this.setState({
            movieToShow: movieDetails,
            isMovieDetailsVisible: true
        });
    }

    hideMovieDetails () {
        this.setState({
            isMovieDetailsVisible: false
        });
    }

    render () {
        return <div className="movie-search-results">
                <ul>
                    {this.props.results.map((movie, movieIndex)=><li key={movieIndex}>
                            <MovieListItem movie={movie} onDetailsRequired={this.showMovieDetails.bind(this)}/>
                        </li>)}
                </ul>
                {this.state.movieToShow && <Modal show={this.state.isMovieDetailsVisible} onHide={this.hideMovieDetails.bind(this)}>
                    <Modal.Header closeButton>
                        {this.state.movieToShow.title} ({this.state.movieToShow.release_date.split('-')[0]})
                    </Modal.Header>
                    <Modal.Body>
                        {this.state.movieToShow.adult ? <Panel header="for adults"  bsStyle="danger">
                            {this.state.movieToShow.overview}
                        </Panel> : this.state.movieToShow.overview}
                    </Modal.Body>
                </Modal>}
            </div>;
    }
}

MovieSearchForm.propTypes = {
    results: React.PropTypes.array
};

module.exports = MovieSearchForm;
