const { expect } = require('chai');
const { shallow } = require('enzyme');
const MovieSearchResults = require('./MovieSearchResults.jsx');
const React = require('react');

const MovieListItem = require('../MovieListItem/MovieListItem.jsx');

describe('<MovieSearchResults />', () => {
    it('should have not MovieListItem tags if the results is an empty array', () => {
        var results = [];
        var component = shallow(<MovieSearchResults results={results}/>);
        expect(component.find(MovieListItem).length).to.be.equal(0);
    });

    it('should have 5 MovieListItem tags if the results is a 5 elements array', () => {
        var results = [{}, {}, {}, {}, {}];
        var component = shallow(<MovieSearchResults results={results}/>);
        expect(component.find(MovieListItem).length).to.be.equal(5);
    });
});