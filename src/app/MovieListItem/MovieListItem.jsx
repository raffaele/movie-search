import React from 'react';
import ReactDOM from 'react-dom';

class MovieListItem extends React.Component {
    constructor (props) {
        super(props);
    }

    showDetails () {
        if (this.props.onDetailsRequired) {
            this.props.onDetailsRequired(this.props.movie);
        }
    }

    render () {
        return <div className="movie-list-item">
                    <strong>Title:</strong>
                    <a className="movie-list-item__title" href="#" onClick={this.showDetails.bind(this)}>{this.props.movie.title}</a>
                    <strong>Year:</strong>
                    <span className="movie-list-item__year">{this.props.movie.release_date.split('-')[0]}</span>
            </div>;
    }
}

MovieListItem.propTypes = {
    onDetailsRequired: React.PropTypes.func
};

module.exports = MovieListItem;
