const { expect } = require('chai');
const { shallow, mount, render } = require('enzyme');
const MovieListItem = require('./MovieListItem.jsx');
const React = require('react');
const sinon = require('sinon');


describe('<MovieListItem />', () => {
    it('should show title of the movie', () => {
        var movie = {
            title: 'my-title',
            release_date: '2020-12-25'
        };
        var component = shallow(<MovieListItem movie={movie}/>);
        expect(component.find('.movie-list-item__title').text()).to.be.equal('my-title');
    });

    it('should show year of production of the movie', () => {
        var movie = {
            title: 'my-title',
            release_date: '2020-12-25'
        };
        var component = shallow(<MovieListItem movie={movie}/>);
        expect(component.find('.movie-list-item__year').text()).to.be.equal('2020');
    });

    it('should call the onDetailsRequired method', () => {
        var movie = {
            title: 'my-title',
            release_date: '2020-12-25'
        };
        var onDetailsRequiredFn = sinon.spy();
        var component = shallow(<MovieListItem movie={movie} onDetailsRequired={onDetailsRequiredFn} />);

        component.find('.movie-list-item__title').simulate('click');
        expect(onDetailsRequiredFn.called).to.be.equal(true);
    });
});
