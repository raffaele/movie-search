import React from 'react';
import ReactDOM from 'react-dom';
import {Pagination} from 'react-bootstrap';

const MovieSearchForm = require('../MovieSearchForm/MovieSearchForm.jsx'),
    MovieSearchResults = require('../MovieSearchResults/MovieSearchResults.jsx'),
    theMovieDb = require('../../theMovieDb.js'),
    {theMovieDbApiKey} = require('../conf.json');



class MovieSearchPage extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            results: [],
            totalPages: 0,
            lastQuery: ''
        };
        theMovieDb.common.api_key = theMovieDbApiKey;
        this.maxPaginationLinks = 5;
    }

    search (query, page = 1) {
        var request = {
            query: query,
            language: 'EN',
            page: page
        };

        theMovieDb.search.getMovie(request, jsonResponse => {
            var response = JSON.parse(jsonResponse);
            this.setState({
                results: response.results,
                errorMsg: null,
                totalPages: response.total_pages,
                activePage: page,
                lastQuery: query
            });
        }, errorMsg => {
            this.setState({
                results: [],
                errorMsg: 'Api issues',
                totalPages: 0,
                activePage: page
            });
        });
    }

    selectPage (selectedPage) {
        this.search(this.state.lastQuery, selectedPage);
    }

    render () {
        var state = this.state;

        return <div className="movie-search-page">
                <MovieSearchForm onSubmit={this.search.bind(this)}></MovieSearchForm>
                <MovieSearchResults results={this.state.results}></MovieSearchResults>
                <div className="movie-search-page__error-msg">{this.state.errorMsg}</div>
                {this.state.totalPages>1 && 
                    <Pagination
                        ellipsis
                        boundaryLinks
                        items={this.state.totalPages}
                        maxButtons={this.maxPaginationLinks}
                        activePage={this.state.activePage}
                        onSelect={this.selectPage.bind(this)} />
                }
            </div>;
    }
}

module.exports = MovieSearchPage;
