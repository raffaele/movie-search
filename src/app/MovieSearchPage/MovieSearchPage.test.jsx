import {Pagination} from 'react-bootstrap';
const { expect } = require('chai');
const { shallow, mount, render } = require('enzyme');
const MovieSearchPage = require('./MovieSearchPage.jsx');
const React = require('react');
const MovieSearchForm = require('../MovieSearchForm/MovieSearchForm');
const MovieSearchResults = require('../MovieSearchResults/MovieSearchResults');
const theMovieDb = require('../../theMovieDb.js');
const sinon = require('sinon');

describe('<MovieSearchPage />', () => {
    var component,
        originalGetMovie;

    beforeEach (() => {
        component = shallow(<MovieSearchPage />);
        originalGetMovie = theMovieDb.search.getMovie;
    });

    afterEach(()=>{
        theMovieDb.search.getMovie = originalGetMovie;
    });

    it('should contain the MovieSearchForm component', () => {
        expect(component.find(MovieSearchForm).length).to.be.equal(1);
    });

    it('should contain the MovieSearchResults component', () => {
        expect(component.find(MovieSearchResults).length).to.be.equal(1);
    });

    it('should send the state.results list to the MovieSearchResults component', () => {
        var finalResults = [{}, {}, {}];
        component.setState({
            results: finalResults
        });
        expect(component.find(MovieSearchResults).node.props.results).to.be.equal(finalResults);
    });

    it('should call the theMovieDb.search.getMovie method and pass the correct params', () => {
        var isGetMovieCalled = false;
        theMovieDb.search.getMovie = (info, successCallback, failCallback) => {
            expect(info).to.be.deep.equal({
                query: 'my-query',
                language: 'EN',
                page: 1
            });
            isGetMovieCalled = true;
        };

        component.instance().search('my-query');
        expect(isGetMovieCalled).to.be.equal(true);
    });

    it('should update state.results on theMovieDb.search.getMovie success', () => {
        var finalResults = {
            results: [{
                title: 'fist title'
            }, {
                title: 'second title'
            }, {
                title: 'third title'
            }],
            total_pages: 25
        };
        theMovieDb.search.getMovie = (info, successCallback, failCallback) => {
            successCallback(JSON.stringify(finalResults))
        };

        component.instance().search('my-title');

        expect(component.state('results')).to.be.deep.equal(finalResults.results);
        expect(component.state('totalPages')).to.be.equal(25);
    });

    it('should set the error state in case of theMovieDb.search.getMovie fails', () => {
        theMovieDb.search.getMovie = (info, successCallback, failCallback) => {
            failCallback({});
        };

        component.instance().search('my-title');

        expect(component.state('errorMsg')).to.be.equal('Api issues');
        expect(component.find('.movie-search-page__error-msg').text()).to.be.equal('Api issues');
    });

    describe('Pagination', () => {
        it('should be present when state.totalPages>1 and should assign the correct item props', () => {
            var totalPages = 5,
                pagination;

            component.setState({totalPages: totalPages});
            pagination = component.find(Pagination);
            expect(pagination.length).to.be.equal(1);
            expect(pagination.node.props.items).to.be.equal(totalPages);
        });

        it('should be not present when state.totalPages = 1 ', () => {
            var totalPages = 1,
                pagination;

            component.setState({totalPages: totalPages});
            pagination = component.find(Pagination);
            expect(pagination.length).to.be.equal(0);
        });
    });
});
