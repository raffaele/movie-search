MOVIE-SEARCH
=======

Simple page to search for movies based on the API: https://www.themoviedb.org/

Dependecies: [NODE](https://nodejs.org/)

To run the project:

1. Download the git repository.
2. Run `npm install` to install the dependencies
3. Run `npm run dev` to launch the project
4. Run `npm test` to execute unit tests.

The [themoviedb.js](https://github.com/cavestri/themoviedb-javascript-library/blob/master/themoviedb.js)
file is included and changed (added just the command `module.exports = themoviedb at the end of the file
to have compatibility with ES6 module.exports).

Unit tests based on [mocha](https://mochajs.org/) and [enzyme](https://github.com/airbnb/enzyme).
